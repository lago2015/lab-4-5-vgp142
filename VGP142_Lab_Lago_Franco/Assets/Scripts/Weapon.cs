﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour {

    public float Countdown = 60f;
    public float hitPoint=1f;
    public float range = 100f;


    public float rateOfFire = 0.5f;
    float coolDownRemaining = 0.0f;
    public int shotsRemaining = 3;

    public int enemy=10;

    //private Animator anim;
    //private AnimatorStateInfo currentBaseState;
    //private AnimatorStateInfo layer2CurrentState;
    //GUI Variables
    int points = 0;
    bool gameOverWin = false;
    bool gameOverLoss = false;

	// Update is called once per frame
	void Update () 
    {
        coolDownRemaining-=Time.deltaTime;
        Countdown -= Time.deltaTime;
        if (Input.GetButtonDown("Fire1"))
        {
            Ray ray = new Ray(Camera.main.transform.position, Camera.main.transform.forward);
            RaycastHit hit;
            //if(shotsRemaining<=0)
            //    anim.SetBool("Reload", true);
            if (Physics.Raycast(ray, out hit, range))
            {
                shotsRemaining--;
                Vector3 hitPoint = hit.point;
                GameObject objHit = hit.collider.gameObject;

                if (objHit.tag == "Enemy")
                {  
                    Destroy(objHit);
                    points++;
                }
                if (points == 10)
                {
                    Time.timeScale = 0;
                    gameOverWin = true;
                }  
            }
            if(shotsRemaining<=0)
            {
                shotsRemaining = 3;
            }
        }
        if(Countdown<=0)
        {
            Time.timeScale = 0;
            gameOverLoss = true;
        }
	}
    void OnGUI()
    {
        GUI.Label(new Rect(50, 40, 100, 25), "Timer: " + Countdown);
        GUI.Label(new Rect(50, 65, 100, 25), "Points: " + points);
        GUI.Label(new Rect(50, 90, 100, 25), "Ammo: " + shotsRemaining + "/3");

        if (gameOverWin)
            GUI.Box(new Rect(Screen.width / 2 - 100, Screen.height/2-100,100,50),"You Win!\n Score: " + points);
       if (gameOverLoss)
           GUI.Box(new Rect(Screen.width / 2 - 100, Screen.height / 2 - 100, 100, 50), "GAME OVER!\n Score: " + points);
    }
}
