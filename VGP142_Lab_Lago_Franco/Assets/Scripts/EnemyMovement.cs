﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour
{

    public Transform targetLocate;
    EnemyMovement agent;
    public Transform SetDestination;

    // Use this for initialization
    void Start()
    {
        agent = GetComponent<EnemyMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        agent.SetDestination = targetLocate;
    }
}