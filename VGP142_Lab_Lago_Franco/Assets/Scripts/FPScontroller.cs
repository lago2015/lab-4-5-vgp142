﻿using UnityEngine;
using System.Collections;

public class FPScontroller : MonoBehaviour {

    [System.NonSerialized]
    public float meshMoveSpeed = 4.0f;

    [System.NonSerialized]
    public float animSpeed = 1.5f;				// a public setting for overall animator animation speed

    private Animator anim;							    // a reference to the animator on the character
    private AnimatorStateInfo currentBaseState;			// a reference to the current state of the animator, used for base layer
    private AnimatorStateInfo layer2CurrentState;	    // a reference to the current state of the animator, used for layer 2

    static int reloadState = Animator.StringToHash("Layer2.Reload");				// and are used to check state for various actions to occur

    static int switchWeaponState = Animator.StringToHash("Layer2.WeaponSwap");

    public float moveSpeed = 10f;
    public float mouseSensitivity = 1.5f;
    public float pitchRange = 60f;
    public float jumpSpeed = 5f;
    public int shotsRemaining = 3;

    float verticalRotation = 0f;
    float verticalVelocity = 0f;

    public GameObject rifle;

    CharacterController control;
	
	// Use this for initialization
	void Start () 
    {
        anim = GetComponent<Animator>();
        if (anim.layerCount == 2)
            anim.SetLayerWeight(1, 1);
        control = GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update ()
    {
	    //Mouse look
        float rotYaw = Input.GetAxis("Mouse X") * mouseSensitivity;
        transform.Rotate(0, rotYaw, 0);

        verticalRotation -= Input.GetAxis("Mouse Y") * mouseSensitivity;
        verticalVelocity = Mathf.Clamp(verticalRotation, -pitchRange, pitchRange);
        Camera.main.transform.localRotation = Quaternion.Euler(verticalRotation, 0, 0);

        //WASD movement

        float forwardSpd = Input.GetAxis("Vertical") * moveSpeed;
        float sideSpd = Input.GetAxis("Horizontal") * moveSpeed;

        verticalVelocity += Physics.gravity.y * Time.deltaTime;

        if(control.isGrounded)
        {
            verticalVelocity = -0.1f;

        }
        if(Input.GetButtonDown("Jump")&&control.isGrounded)
        {
            verticalVelocity = jumpSpeed;
        }

        //Puts all the movement script together
        Vector3 speed = new Vector3(sideSpd, verticalVelocity, forwardSpd);
        speed = transform.rotation * speed;

        control.Move(speed * Time.deltaTime);

	}
    void FixedUpdate()
    {
        float h = Input.GetAxis("Horizontal");				// setup h variable as our horizontal input axis
        float v = Input.GetAxis("Vertical");				// setup v variables as our vertical input axis
        anim.SetFloat("Speed", v);							// set our animator's float parameter 'Speed' equal to the vertical input axis				
        anim.SetFloat("Direction", h); 						// set our animator's float parameter 'Direction' equal to the horizontal input axis		
        anim.speed = animSpeed;								// set the speed of our animator to the public variable 'animSpeed'
        //anim.SetLookAtWeight(lookWeight);					// set the Look At Weight - amount to use look at IK vs using the head's animation
        currentBaseState = anim.GetCurrentAnimatorStateInfo(0);	// set our currentState variable to the current state of the Base Layer (0) of animation

        //Controls the movement speed
        if (v <= 0.0f)
        {
            meshMoveSpeed = 4;
        }
        else
        {
            meshMoveSpeed = 6;
        }

        if (anim.layerCount == 2)
        {
            layer2CurrentState = anim.GetCurrentAnimatorStateInfo(1);	// set our layer2CurrentState variable to the current state of the second Layer (1) of animation
        }
        //Reload weapon state
        if (Input.GetButtonDown("Fire2"))
        {
            anim.SetBool("Reload", true);   
        }
        else
        {
            anim.SetBool("Reload", false);
        }
        //Switch weapon state
        //if (layer2CurrentState.nameHash != reloadState || currentBaseState.nameHash != switchWeaponState)
        //{
        //    anim.SetBool("SwitchWeapon", true);
        //}
        if (layer2CurrentState.nameHash == switchWeaponState)
        {
            anim.SetBool("SwitchWeapon", false);
        }
    }
}
